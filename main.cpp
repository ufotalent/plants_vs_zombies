#include "io.h"
#include "game.h"
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance,PSTR szCmdLine, int iCmdShow)
{
	InitGame();
	while (1)
	{
		int Mode=SelectMode();
		if (Mode==MD_QUIT)
			return 0;
		if (Mode==MD_ADVENTURE)
			StartAdventure();
	}
} 