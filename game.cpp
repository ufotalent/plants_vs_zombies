#pragma warning (disable:4786)
#include "game.h"
#include "io.h"
#include "type.h"
#include <fstream>
#include <strstream>
#include <map>
#include <string>
#include <algorithm>
using namespace std;
vector <ZOMBIE> Zombies;
vector <PLANT> Plants;
vector <BULLET> Bullets;
map <string,ZOMBIE> ZombieDict;
map <string,PLANT> PlantDict;
map <string,BULLET> BulletDict;
int Money;
int MaxTime;
vector <int> ZombieTime;
vector <string> ZombieID;
string BackGroundID;
string MusicName;
int MaxRow,MaxCol,MatrixStartX,MatrixStartY,MatrixX,MatrixY;
int CurrentTime;
int CurrentZombie;
int GameSpeed;
int BonusCD;
bool CoreEnded;
bool CoreRunning;
bool CoreWaiting;
bool ShovelChosen;
bool MenuButtonHighlighted;
bool ShovelHighlighted; 
bool ControlRunning;
string StageName;


void UserControl();
void LoadStage(int stage);
void AddPlant(string ID,int row,int col);
void AddZombie(string ID,int row);
bool operator <(PLANT a,PLANT b);
bool operator <(ZOMBIE a,ZOMBIE b);
bool Ready(PLANT data);
void ClearData();
void UserLost();
void UserWin();

bool Choosing;
string ChoosingID;

void InitGame()
{
	InitGraph();
	SetWindowTitle("");
	PlaySound(BACKGROUNDMUSIC,NULL,SND_FILENAME|SND_ASYNC|SND_LOOP);
	ifstream fin(DATALIST);
	int number;
	fin>>number;
	LoadImage("wallnut","data\\wallnut.bmp");
	LoadImage("title","data\\title.bmp");
	LoadImage("loadinggrass","data\\loadinggrass.bmp");
	LoadImage("logo","data\\logo.bmp");
	for (int y=-80;y<=20;y+=2)
	{
		SuspendDrawing();
		PutImage("title",0,0);
		PutImage("logo",50,y);
		ResumeDrawing();
	}
	CreateImage("temp",1,1);
	for (int i=1;i<=number;i++)
	{

		string type;
		fin>>type;
		if (type.compare("#")==0)
			break;
		if (type.compare("I")==0)
		{
			string ID,filename;
			fin>>ID>>filename;
			LoadImage(ID,filename);
		}
		if (type.compare("A")==0)
		{
			string ID,filename;
			fin>>ID>>filename;
			LoadAVI(ID,filename);
		}
		for (int j=300.0/number*(i-1)+200;j<=300.0/number*i+200;j+=1)
		{
			if (!CopyImage("wallnut","temp"))
				return ;
			RotateImage("temp",50,50,(-(j-300)/25.0));
			SuspendDrawing();
			PutImage("title",0,0);
			PutImage("loadinggrass",220,480);
			PutImage("temp",j,400);
			PutImage("logo",50,20);
			ResumeDrawing();
		}
	}
	DeleteImage("wallnut");
	DeleteImage("title");
	DeleteImage("loadinggrass");
	DeleteImage("logo");
	DeleteImage("temp");
	fin.close();
	fin.open("data\\Plants.txt");
	while (1)
	{
		string ID;
		PLANT plant;
		fin>>ID;
		if (ID[0]=='#')
			break;
		fin>>plant.HP>>plant.interval>>plant.CoolDown>>plant.ImageID>>plant.BulletID>>plant.Price>>plant.SeedID;
		plant.SeedHighlighted=false;
		plant.TypeID=ID;
		PlantDict[ID]=plant;
	}
	fin.close();
	fin.open("data\\Zombies.txt");
	while (1)
	{
		string ID;
		ZOMBIE zombie;
		fin>>ID;
		if (ID[0]=='#')
			break;
		fin>>zombie.HP>>zombie.speed>>zombie.attack>>zombie.AVIID>>zombie.attackAVIID;
		zombie.TypeID=ID;
		ZombieDict[ID]=zombie;

	}
	fin.close();
	fin.open("data\\Bullets.txt");
	while (1)
	{
		string ID;
		BULLET bullet;
		fin>>ID;
		if (ID[0]=='#')
			break;
		fin>>bullet.attack>>bullet.speed>>bullet.ImageID;
		bullet.TypeID=ID;
		BulletDict[ID]=bullet;
	}
	fin.close();
}
int SelectMode()
{
	for (int i=600;i>=40;i-=10)
	{
		SuspendDrawing();
		PutImage("SelectorScreen_BG",0,0);
		PutImage("SelectorScreen_BG_Center",80,250);
		PutImage("SelectorScreen_BG_Left",0,-80);
		PutImage("SelectorScreen_BG_Right",70,i);
		PutImage("StartAdventure",406,i+25);
		ResumeDrawing();
	}
	CreateImage("LightedQuit",1,1);
	CreateImage("LightedStartAdventure",1,1);
	CopyImage("Quit","LightedQuit");
	CopyImage("StartAdventure","LightedStartAdventure");
	HighlightImage("LightedQuit");
	HighlightImage("LightedStartAdventure");
	SuspendDrawing();
	PutImage("StartAdventure",406,65);
	PutImage("Quit",716,505);
	ResumeDrawing();
	while (1)
	{
		MOUSESTRUCT mouse;
		mouse=GetMouse();
		MOUSESTRUCT ms=GetLastClick();
		bool quit=false,adv=false;
		for (int dx=-3;dx<=3;dx++)
			for (int dy=-3;dy<=3;dy++)
			{
				if (CheckPosition(mouse.x+dx,mouse.y+dy,"Quit"))
				{
					quit=true;
				}
				if (ms.left)
				{
					if(CheckPosition(ms.x+dx,ms.y+dy,"Quit"))
						return MD_QUIT;
					if(CheckPosition(ms.x+dx,ms.y+dy,"StartAdventure"))
						return MD_ADVENTURE;
				}
				if (CheckPosition(mouse.x+dx,mouse.y+dy,"StartAdventure"))
				{
					adv=true;
				}
			}
		SuspendDrawing();
		if (quit)
			PutImage("LightedQuit",716,505);
		else
		{
			PutImage("Quit",716,505);
		}
		if (adv)
			PutImage("LightedStartAdventure",406,65);
		else
		{
			PutImage("StartAdventure",406,65);
		}
		ResumeDrawing();
	}

	return -1;
}
void StartAdventure()
{
	int stage;
	ifstream fin("userinfo.txt");	
	if (!fin)
	{
		stage=1;
		fin.close();
	}
	else
	{
		fin>>stage;
		fin.close();
	}
	LoadStage(stage);
	ClearData();
	StartGameCore();
	UserControl();
}
void UserControl()
{
	SYSTEMTIME st;
	GetSystemTime(&st);
	srand(st.wMilliseconds);
	while (1)
	{
		while (CoreRunning)
		{ 
			Sleep(0);
		}
		ControlRunning=true;
		MOUSESTRUCT ms=GetMouse();
		MOUSESTRUCT click=GetLastClick();
		if (CurrentZombie<ZombieTime.size()&&(CurrentTime>=ZombieTime[CurrentZombie]))
		{
			AddZombie(ZombieID[CurrentZombie],int(rand()/(RAND_MAX+1.0)*MaxRow+1));
			CurrentZombie++;
		}

		for (map<string,PLANT>::iterator it=PlantDict.begin();it!=PlantDict.end();it++)
		{
			if (Ready(it->second))
			{
				if (CheckPosition(ms.x,ms.y,it->second.SeedID))
				{
					it->second.SeedHighlighted=true;
					if (click.left&&!ShovelChosen)
					{
						Choosing=true;
						ChoosingID=it->second.TypeID;
						click.left=false;
					}
				}
				else
					it->second.SeedHighlighted=false;
			}
		}
		if (CheckPosition(ms.x,ms.y,"MenuButton"))
			MenuButtonHighlighted=true;
		else
			MenuButtonHighlighted=false;
		if (CheckPosition(ms.x,ms.y,"Shovel"))
			ShovelHighlighted=true;
		else
			ShovelHighlighted=false;
		if (click.right)
		{
			Choosing=false;
			ShovelChosen=false;
		}
		if (click.left)
		{
			if (Choosing)
			{
				int row=(click.y-MatrixStartY)/MatrixY+1;
				int col=(click.x-MatrixStartX)/MatrixX+1;
				bool can=true;
				for (vector<PLANT>::iterator it=Plants.begin();it!=Plants.end();it++)
					if (it->row==row&&it->col==col)
					{
						can=false;
						break;
					}
				if (can)
				{
					AddPlant(ChoosingID,row,col);
					Money-=PlantDict[ChoosingID].Price;
					if (Money<0)
						Money=Money;
					Choosing=false;
				}
			}
			if (ShovelChosen)
			{
				for (vector<PLANT>::iterator it=Plants.begin();it!=Plants.end();it++)
					if (CheckPosition(ms.x,ms.y,it->ImageID))
					{
						DeleteImage(it->ImageID);
						Plants.erase(it);
						break;
					}
				ShovelChosen=false;
			}
			else
				if (!Choosing)
				{
					if (CheckPosition(click.x,click.y,"Shovel"))
					{
						ShovelChosen=true;
					}
					else
						ShovelChosen=false;
				}
			if (CheckPosition(click.x,click.y,"MenuButton"))
			{
				EndGameCore(1);
				CreateImage("Menu_BackToGame_highlight",1,1);
				CreateImage("Menu_BackToMainMenu_highlight",1,1);
				CreateImage("Menu_Restart_highlight",1,1);
				CopyImage("Menu_BackToGame","Menu_BackToGame_highlight");
				CopyImage("Menu_BackToMainMenu","Menu_BackToMainMenu_highlight");
				CopyImage("Menu_Restart","Menu_Restart_highlight");
				HighlightImage("Menu_BackToGame_highlight");
				HighlightImage("Menu_BackToMainMenu_highlight");
				HighlightImage("Menu_Restart_highlight");
				while (1)
				{
					SuspendDrawing();
					PutImage("Menu",226,70);
					MOUSESTRUCT ms=GetMouse();
					if (CheckPosition(ms.x,ms.y,"Menu_BackToGame"))
						PutImage("Menu_BackToGame_highlight",253,385);
					else
						PutImage("Menu_BackToGame",253,385);
					if (CheckPosition(ms.x,ms.y,"Menu_BackToMainMenu"))
						PutImage("Menu_BackToMainMenu_highlight",304,330);
					else
						PutImage("Menu_BackToMainMenu",304,330);
					if (CheckPosition(ms.x,ms.y,"Menu_Restart"))
						PutImage("Menu_Restart_highlight",304,285);
					else
						PutImage("Menu_Restart",304,285);
					ResumeDrawing();

					MOUSESTRUCT click=GetLastClick();
					if (click.left)
					{
						if (CheckPosition(click.x,click.y,"Menu_BackToGame"))
						{
							StartGameCore();
							break;
						}
						if (CheckPosition(click.x,click.y,"Menu_BackToMainMenu"))
						{
							PlaySound(BACKGROUNDMUSIC,NULL,SND_FILENAME|SND_ASYNC|SND_LOOP);
							return;
						}
						if (CheckPosition(click.x,click.y,"Menu_Restart"))
						{
							ClearData();
							StartGameCore();
							break;
						}
					}
				}
				DeleteImage("Menu_BackToGame_highlight");
				DeleteImage("Menu_BackToMainMenu_highlight");
			}
		}
		
		if  (Zombies.size()==0)
		{
			if (CurrentZombie==ZombieTime.size())
			{
				EndGameCore(1000);
				UserWin();
				StartGameCore();
			}
		}
		for (vector<ZOMBIE>::iterator zmit=Zombies.begin();zmit!=Zombies.end();zmit++)
		{
			if (zmit->x<-50)
			{
				EndGameCore(1);
				UserLost();
				PlaySound(BACKGROUNDMUSIC,NULL,SND_FILENAME|SND_ASYNC|SND_LOOP);
				return;
			}
		}
		ControlRunning=false;
		Sleep(10);
	}
}

void LoadStage(int stage)
{
	ifstream fin;
	strstream str;
	str<<"data\\stage"<<stage<<".txt";
	string filename;
	str>>filename;
	fin.open(filename.c_str());
	int time;
	string ID;
	char temp[100];
	fin.getline(temp,100);
	StageName=temp;
	fin>>::BackGroundID>>MaxCol>>MaxRow>>MatrixStartX>>MatrixStartY>>MatrixX>>MatrixY>>GameSpeed;
	fin>>MusicName;
	PlaySound(MusicName.c_str(),NULL,SND_FILENAME|SND_ASYNC|SND_LOOP);
	ZombieTime.clear();
	ZombieID.clear();
	while (fin>>time)
	{
		fin>>ID;
		ZombieTime.insert(ZombieTime.end(),time);
		if (time>MaxTime)
			MaxTime=time;
		ZombieID.insert(ZombieID.end(),ID);
	}
	fin.close();
}

void GameCore()
{
	int i;
	map <string,PLANT>::iterator it;	
	vector <PLANT>::iterator plit;
	vector <ZOMBIE>::iterator zmit;
	SetWindowTitle(StageName);
	if (CoreEnded)
		return;
	CoreWaiting=true;
	while (CoreRunning)
	{
		Sleep(1);
	}
	CoreWaiting=false;
	if (Money<0)
		Money=0;
	if (Money>9999)
		Money=9999;
	CoreRunning=true;
	CurrentTime+=GameSpeed;
	if (CurrentTime/100!=(CurrentTime-GameSpeed)/100)
	{
		Money+=15;
		if (Money<0)
			Money=Money;
	}
	BonusCD-=GameSpeed;
	SuspendDrawing();
	PutImage(BackGroundID,0,0);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	draw control panel
//
	PutImage("ControlPanel",0,0);
	int placex=76;
	int placey=7;
	for (it=PlantDict.begin();it!=PlantDict.end();it++)
	{
		string name;
		if (it->second.SeedHighlighted)
		{
			if (CreateImage(it->second.SeedID+"_highlight",1,1))
			{
				CopyImage(it->second.SeedID,it->second.SeedID+"_highlight");
				HighlightImage(it->second.SeedID+"_highlight");
			}
			name=it->second.SeedID+"_highlight";
		}
		else
			name=it->second.SeedID;
		PutImage(name,placex,placey);
		if (Money<it->second.Price)
			DeepenRect(placex,placey,placex+50,placey+72);
		else
			DeepenRect(placex,placey,placex+50,placey+72.0*it->second.CurrentCoolDown/it->second.CoolDown);
		placex+=50;
	}
	PutImage("ShovelBank",500,0);
	if (!ShovelChosen)
		if (ShovelHighlighted)
		{
			if (CreateImage("Shovel_highlight",1,1))
			{
				CopyImage("Shovel","Shovel_highlight");
				HighlightImage("Shovel_highlight");
			}
			PutImage("Shovel_highlight",490,0);
		}
		else
			PutImage("Shovel",490,0);

	if (MenuButtonHighlighted)
		{
			if (CreateImage("MenuButton_highlight",1,1))
			{
				CopyImage("MenuButton","MenuButton_highlight");
				HighlightImage("MenuButton_highlight");
			}
			PutImage("MenuButton_highlight",600,1);
		}
	else
		PutImage("MenuButton",600,1);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	
//	draw money number
//
	placey=62;
	placex=46;
	int temp=Money;
	if (temp==0)
		PutImage("0",placex,placey);
	while (temp)
	{
		int now=temp%10;
		string a="0";
		a[0]=now-0+'0';
		PutImage(a,placex,placey);
		placex-=10;
		temp=temp/10;
	}	
	//drawplants
	
	for (plit=Plants.begin();plit!=Plants.end();plit++)
	{
		if (!ShovelChosen)
		{
			if (plit->TypeID.compare("CherryBomb")||plit->Exploding)
				PutImage(plit->ImageID,(plit->col-1)*MatrixX+MatrixStartX,(plit->row-1)*MatrixY+MatrixStartY-30);
			else
				PutImage(plit->ImageID,(plit->col-1)*MatrixX+MatrixStartX+rand()/(1.0+RAND_MAX)*5,(plit->row-1)*MatrixY+MatrixStartY-30+rand()/(1.0+RAND_MAX)*5);
		}
		else
		{
			MOUSESTRUCT ms=GetMouse();
			if (CheckPosition(ms.x,ms.y,plit->ImageID))
			{
				if (CreateImage(plit->ImageID+"_highlight",1,1))
				{
					CopyImage(plit->ImageID,plit->ImageID+"_highlight");
					HighlightImage(plit->ImageID+"_highlight");
				}
				PutImage(plit->ImageID+"_highlight",(plit->col-1)*MatrixX+MatrixStartX,(plit->row-1)*MatrixY+MatrixStartY-30);
			}
			else
				if (plit->TypeID.compare("CherryBomb")||plit->Exploding)
					PutImage(plit->ImageID,(plit->col-1)*MatrixX+MatrixStartX,(plit->row-1)*MatrixY+MatrixStartY-30);
				else
					PutImage(plit->ImageID,(plit->col-1)*MatrixX+MatrixStartX+rand()/(1.0+RAND_MAX)*5,(plit->row-1)*MatrixY+MatrixStartY-30+rand()/(1.0+RAND_MAX)*5);
		}
	}
	//drawzombies
	for (zmit=Zombies.begin();zmit!=Zombies.end();zmit++)
	{
		if (!zmit->attacking)
		{
			zmit->frame+=zmit->speed*(rand()/(double)RAND_MAX*0.2+1)*GameSpeed;
			if (zmit->frame>GetAVIFrameCount(zmit->AVIID))
				zmit->frame=1;
			PutAVI(zmit->AVIID,zmit->frame,zmit->x,(zmit->row-1)*MatrixY+MatrixStartY+MatrixY/2-120);
		}
		else
		{
			zmit->attframe+=zmit->speed*(rand()/(double)RAND_MAX*0.2+1)*GameSpeed;
			if (zmit->attframe>GetAVIFrameCount(zmit->attackAVIID))
				zmit->attframe=1;
			PutAVI(zmit->attackAVIID,zmit->attframe,zmit->x,(zmit->row-1)*MatrixY+MatrixStartY+MatrixY/2-120);
		}

	}
	//draw the plant to plant
	if (Choosing)
	{
		MOUSESTRUCT ms=GetMouse();
		int row=(ms.y-MatrixStartY)/MatrixY+1;
		int col=(ms.x-MatrixStartX)/MatrixX+1;
		if (CreateImage(ChoosingID+"_highlight",1,1))
		{
			CopyImage(ChoosingID,ChoosingID+"_highlight");
			HighlightImage(ChoosingID+"_highlight");
			HighlightImage(ChoosingID+"_highlight");
		}
		PutImage(ChoosingID+"_highlight",(col-1)*MatrixX+MatrixStartX,(row-1)*MatrixY+MatrixStartY-30);
		PutImage(ChoosingID,ms.x-50,ms.y-70);
	}
	//draw bullets
	vector<BULLET>::iterator blit;
	for (blit=Bullets.begin();blit!=Bullets.end();blit++)
	{
		PutImage(blit->ImageID,blit->x,(blit->row-1)*MatrixY+MatrixStartY-20);
	}
	
	if (ShovelChosen)
	{
		MOUSESTRUCT ms=GetMouse();
		PutImage("Shovel",ms.x-40,ms.y-40);
	}
	//draw process bar
	DrawProcessBar(620,540,CurrentTime*1.0/MaxTime);
	PutImage("LevelProcess",655,555);
	DrawProcessBar(20,540,1-BonusCD*1.0/BONUSINTERVAL);
	PutImage("Bonus",20,510);
	ResumeDrawing();
	//zombies act
	for (zmit=Zombies.begin();zmit!=Zombies.end();zmit++)
	{
		bool att=false;
		if (zmit->slow>0)
		{
			zmit->slow-=GameSpeed;
			if (zmit->slow<=0)
			{
				zmit->speed*=2;
			}
		}
		for (int i=0;i<Plants.size();i++)
		{
			if (zmit->row==Plants[i].row)
			{
				if (zmit->x<=(Plants[i].col-1)*MatrixX+MatrixStartX+50&&zmit->x>=(Plants[i].col-2)*MatrixX+MatrixStartX+50)
				{
					att=true;
					Plants[i].HP-=zmit->speed*zmit->attack*GameSpeed;
					if (Plants[i].HP<=0)
					{
						DeleteImage((Plants.begin()+i)->ImageID);
						Plants.erase(Plants.begin()+i);
					}
					break;
				}
			}
		}
		zmit->attacking=att;
		if (!zmit->attacking)zmit->x=zmit->x-zmit->speed*GameSpeed;
	}

	//plants act
	for (plit=Plants.begin();plit!=Plants.end();plit++)
	{
		plit->interval-=GameSpeed;
		if (plit->TypeID.compare("CherryBomb")==0&&plit->interval<=15)
		{
			plit->Exploding=true;
			plit->ImageID="Bang";
			for (int i=0;i<Zombies.size();i++)
			{
				if ((plit->row-Zombies[i].row)*(plit->row-Zombies[i].row)<=1  &&
					(plit->col-2.5)*MatrixX+MatrixStartX<=Zombies[i].x	&&
					(plit->col+1)*MatrixX+MatrixStartX>=Zombies[i].x
					)
				{
					Zombies.erase(Zombies.begin()+i);
					i--;
				}
			}
		}
		if (plit->interval<=0)
		{
			for (int i=0;i<Zombies.size();i++)
				if (plit->row==Zombies[i].row&&
					(plit->col-1)*MatrixX+MatrixStartX+50<Zombies[i].x)
				{
					plit->interval=PlantDict[plit->TypeID].interval*(rand()/(double)RAND_MAX*0.2+1);
					BULLET bullet=BulletDict[plit->BulletID];
					bullet.row=plit->row;
					bullet.x=(plit->col-1)*MatrixX+MatrixStartX+50;
					Bullets.insert(Bullets.end(),bullet);
					break;
				}
		}
	}
	for (i=0;i<Plants.size();i++)
	{
		if (Plants[i].TypeID.compare("CherryBomb")==0)
		{
			if (Plants[i].interval<=GameSpeed)
			{
				Plants.erase(Plants.begin()+i);
				i--;
			}
		}
	}
	//bullets act
	for ( i=0;i<Bullets.size();i++)
	{
		Bullets[i].x+=Bullets[i].speed*GameSpeed;
		if (Bullets[i].x>MAXSCREENX)
		{
			Bullets.erase(Bullets.begin()+i);
			i--;
			continue;
		}
		for (int j=0;j<Zombies.size();j++)
		{
			if (Bullets[i].row==Zombies[j].row)
			{
				if ((Bullets[i].x-Zombies[j].x)*(Bullets[i].x-Bullets[i].speed*GameSpeed-(Zombies[j].x+Zombies[j].speed*GameSpeed))<=0)
				{
					Zombies[j].HP-=Bullets[i].attack;
					if (Bullets[i].TypeID.compare("SnowPea")==0)
					{
						if (!Zombies[j].slow)
							Zombies[j].speed*=0.5;
						Zombies[j].slow=300;
					}
					if (Zombies[j].HP<=0)
					{
						if (BonusCD<=0)
						{
							BonusCD=BONUSINTERVAL;
							Money+=ZombieDict[Zombies[j].TypeID].HP/3;
							if (Money<0)
								Money=Money;
						}
						Zombies.erase(Zombies.begin()+j);
					}
					Bullets.erase(Bullets.begin()+i);
					i--;
					break;
				}
			}
		}
	}
	//Cool down decrease
	for (it=PlantDict.begin();it!=PlantDict.end();it++)
	{
		if (it->second.CurrentCoolDown>0)
			it->second.CurrentCoolDown-=GameSpeed;
	}
	CoreRunning=false;
}
bool operator <(PLANT a,PLANT b)
{
	if (a.row<b.row)
		return true;
	else
		return false;
}
bool operator <(ZOMBIE a,ZOMBIE b)
{
	if (a.row<b.row)
		return true;
	else
		return false;
}
void AddPlant(string ID,int row,int col)
{
	static int id=0;
	id++;
	strstream str;
	str<<id;
	string s;
	str>>s;
	PLANT plant=PlantDict[ID];
	plant.ImageID+=s;
	CreateImage(plant.ImageID,1,1);
	CopyImage(PlantDict[ID].ImageID,plant.ImageID);
	plant.col=col;
	plant.row=row;
	plant.Exploding=false;
	PlantDict[ID].CurrentCoolDown=PlantDict[ID].CoolDown;
	Plants.insert(Plants.end(),plant);
}
void AddZombie(string ID,int row)
{
	ZOMBIE zombie=ZombieDict[ID];
	zombie.speed=zombie.speed*(rand()/(double)RAND_MAX*0.2+1);
	zombie.attacking=false;	
	zombie.row=row;
	zombie.frame=1;
	zombie.attframe=1;
	zombie.x=810;
	zombie.slow=0;
	Zombies.insert(Zombies.begin(),zombie);
	sort(Zombies.begin(),Zombies.end());
}
bool Ready(PLANT data)
{
	if (data.Price<=Money&&data.CurrentCoolDown<=0)		
		return true;
	else
		return false;
}
void ClearData()
{
	Zombies.clear();
	Plants.clear();
	Bullets.clear();
	for (map<string,PLANT>::iterator it=PlantDict.begin();it!=PlantDict.end();it++)
		it->second.CurrentCoolDown=it->second.CoolDown;
	Money=100;
	CurrentTime=0;
	CurrentZombie=0;
	ShovelChosen=false;
	MenuButtonHighlighted=false;
	Choosing=false;
	BonusCD=BONUSINTERVAL;
	CoreRunning=false;
	ControlRunning=false;
}
void UserLost()
{
	PlaySound(USERLOSTMUSIC,NULL,SND_FILENAME|SND_ASYNC);
	SuspendDrawing();
	PutImage("UserLost",118,66);
	ResumeDrawing();
	GetLastClick();
	GetLastKey();
	while (!(GetLastClick().left||GetLastKey()));
}
void UserWin()
{
	PlaySound(USERWINMUSIC,NULL,SND_FILENAME|SND_ASYNC);
	CreateImage("NextLevel_highlight",1,1);
	CopyImage("NextLevel","NextLevel_highlight");
	HighlightImage("NextLevel_highlight");
	while (1)
	{
		SuspendDrawing();
		PutImage("WinBox",245,150);
		PutImage("UserWin",280,200);
		MOUSESTRUCT ms=GetMouse();
		if (CheckPosition(ms.x,ms.y,"NextLevel"))
			PutImage("NextLevel_highlight",268,328);
		else
			PutImage("NextLevel",268,328);
		MOUSESTRUCT click=GetLastClick();
		if (click.left&&CheckPosition(click.x,click.y,"NextLevel"))
			break;
		ResumeDrawing();
	}
	DeleteImage("NextLevel_highlight");
	int stage;
	ifstream fin("userinfo.txt");
	if (!fin)
		stage=1;
	else
		fin>>stage;
	fin.close();
	stage++;
	if (stage>MAXSTAGE)
	{
		stage=1;
	}
	ofstream fout("userinfo.txt");
	fout<<stage;
	fout.close();
	LoadStage(stage);
	ClearData();
}