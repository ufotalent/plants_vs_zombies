#pragma warning (disable:4786)
#include "io.h"
#include "game.h"
HWND hWnd;
HDC hdcWindow,hdcBuffer;
HBITMAP hBitmapBuffer;
MOUSESTRUCT ms,LastClick;
map<string,IMAGE> ImageData;
map<string,AVI> AVIData;
bool bKey[128];
bool DrawingSuspended=false;
bool Refreshing;
int iLastKey;
void createWindow(void * pParam);
extern bool CoreRunning;
extern bool ControlRunning;
extern bool CoreWaiting;
LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
void createWindow(void * pParam)
{    
    static char szAppName[] = WINDOWNAME;
    MSG    msg ;
    WNDCLASS wndclass ;
	wndclass.style        = CS_HREDRAW | CS_VREDRAW ;
	wndclass.lpfnWndProc  = WndProc ;    
	wndclass.cbClsExtra   = 0 ;   
    wndclass.cbWndExtra   = 0 ;
    wndclass.hInstance    = NULL ;
    wndclass.hIcon        = LoadIcon (NULL, IDI_APPLICATION) ;        
	wndclass.hCursor      = LoadCursor (NULL, IDC_ARROW) ;    
	wndclass.hbrBackground= (HBRUSH) GetStockObject (WHITE_BRUSH) ;    
	wndclass.lpszMenuName = NULL ;
    wndclass.lpszClassName= szAppName ;
    if (!RegisterClass (&wndclass))
    {
		MessageBox(NULL,"This program requires Windows NT!",szAppName, MB_ICONERROR) ;
        return ; 
    }
    hWnd = CreateWindow( szAppName,      // window class name
                   WINDOWNAME,   // window caption
                   WS_SYSMENU,  // window style
                   CW_USEDEFAULT,// initial x position
                   CW_USEDEFAULT,// initial y position
                   MAXSCREENX,// initial x size
                   MAXSCREENY,// initial y size
                   NULL,                 // parent window handle
				   NULL,            // window menu handle
				   NULL,   // program instance handle
				   NULL) ;      // creation parameters                   
    ShowWindow (hWnd, SW_SHOWNORMAL) ;
    UpdateWindow (hWnd) ;
    while (GetMessage (&msg, NULL, 0, 0))
    {
		TranslateMessage (&msg);
		DispatchMessage (&msg) ;
    }
    return ;
}
        

LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
        
{
	
    switch (message)
    {
    case	WM_CREATE:
				//fill the map with black color
				hdcWindow=GetDC(hwnd);
				int *bits;
				bits=(int*) malloc(sizeof(int)*MAXSCREENX*MAXSCREENY);
				hBitmapBuffer=CreateBitmap(MAXSCREENX,MAXSCREENY,1,32,bits);
				hdcBuffer=CreateCompatibleDC(hdcWindow);
				SelectObject(hdcBuffer,hBitmapBuffer);
				break;
	case	WM_PAINT:
				BitBlt(hdcWindow,0,0,MAXSCREENX,MAXSCREENY,hdcBuffer,0,0,SRCCOPY);
	case	WM_MOUSEMOVE:
				ms.x=LOWORD(lParam);
				ms.y=HIWORD(lParam);
				break;
	case	WM_LBUTTONUP:
				ms.left=false;
				LastClick=ms;
				LastClick.right=false;
				LastClick.left=true;
				break;
	case	WM_RBUTTONUP:
				ms.right=false;
				LastClick=ms;
				LastClick.right=true;
				LastClick.left=false;
				break;
	case	WM_LBUTTONDOWN:
				ms.left=true;
				break;
	case	WM_RBUTTONDOWN:
				ms.right=true;
				break;
	case	WM_KEYDOWN:
				bKey[wParam]=true;
				iLastKey=wParam;
				break;
	case	WM_KEYUP:
				bKey[wParam]=false;
				break;
	case	WM_TIMER:
				switch(wParam)
				{
					case	TIMER_REFRESH:
								if (!DrawingSuspended)
								{
									Refreshing=true;
									BitBlt(hdcWindow,0,0,MAXSCREENX,MAXSCREENY,hdcBuffer,0,0,SRCCOPY);
									Refreshing=false;
								}
								break;
					case 	TIMER_GAMECORE:
								GameCore();
				}
				break;
    case	WM_DESTROY:
				exit (0) ;
    }
  return DefWindowProc (hwnd, message, wParam, lParam) ; 
}
void InitGraph()
{
	_beginthread(createWindow,0,NULL);
	while (!hWnd)
		Sleep(1);
	while (!hdcWindow)
		Sleep(1);
	SetTimer(hWnd,TIMER_REFRESH,INTERVAL,NULL);
}

int GetLastKey()
{
	int temp=iLastKey;
	if (iLastKey!=0)
	{
		int i=1;
	}
	iLastKey=0;
	return temp;
}
MOUSESTRUCT GetMouse()
{
	return ms;
}
bool LoadImage(string ID,string FileName)
{
	FILE *fp;
	int width,height;
	if (ImageData.find(ID)!=ImageData.end())
		return false;
	if ((fp=fopen(FileName.c_str(),"rb"))==NULL)
		return false;
	BITMAPFILEHEADER  bfHeader;
	BITMAPINFOHEADER  biHeader;
	fread(&bfHeader,sizeof(bfHeader),1,fp);
	fread(&biHeader,sizeof(biHeader),1,fp);
	fclose(fp);
	width=biHeader.biWidth;
	height=biHeader.biHeight;
	HBITMAP hBitmaptemp=(HBITMAP)LoadImage(NULL,FileName.c_str(),IMAGE_BITMAP,width,height,LR_LOADFROMFILE);
	if (hBitmaptemp)
	{
		ImageData[ID].hBitmap=hBitmaptemp;
		ImageData[ID].cx=width;
		ImageData[ID].cy=height;
		ImageData[ID].x=-1;
		ImageData[ID].y=-1;
		ImageData[ID].showed=false;
		return true;
	}
	else
		return false;
}
bool PutImage(string ID,int x,int y)
{
	map<string,IMAGE>::iterator it;
	if ((it=ImageData.find(ID))==ImageData.end())
		return false;
	it->second.x=x;
	it->second.y=y;
	it->second.showed=true;
	HBITMAP hBitmap=it->second.hBitmap;
	HDC hdc;
	while ((hdc=CreateCompatibleDC(hdcWindow))==NULL)
		Sleep(1);
	SelectObject(hdc,hBitmap);
	//while (Refreshing);
	while (!TransparentBlt(hdcBuffer,x,y,it->second.cx,it->second.cy,hdc,0,0,it->second.cx,it->second.cy,0xffffff))
		Sleep(1);
	DeleteDC(hdc);
	return true;
}
bool DeleteImage(string ID)
{
	map<string,IMAGE>::iterator it;
	if ((it=ImageData.find(ID))==ImageData.end())
		return false;
	DeleteObject(ImageData[ID].hBitmap);
	ImageData.erase(it);
	return true;
}
void SuspendDrawing()
{
	DrawingSuspended=true;
	while (Refreshing)
		Sleep(1);
}
void ResumeDrawing()
{
	DrawingSuspended=false;
	Refresh();
}
void Refresh()
{
	Refreshing=true;	
	BitBlt(hdcWindow,0,0,MAXSCREENX,MAXSCREENY,hdcBuffer,0,0,SRCCOPY);
	Refreshing=false;
}
bool HighlightImage(string ID)/**/
{
	map<string,IMAGE>::iterator it;
	if ((it=ImageData.find(ID))==ImageData.end())
		return false;
	HBITMAP hBitmap=it->second.hBitmap;
	int size=it->second.cx*it->second.cy*sizeof(int);
	int *bits=(int*)malloc(size);
	GetBitmapBits(hBitmap,size,bits);
	for (int *p=bits;p<bits+size/sizeof(int);p++)
	{
		if ((*p)!=0xffffff)
		{
			int B=*p>>16;
			int G=(*p>>8)&0xff;
			int R=*p&0xff;
			R=sqrt((double)R*0xff);
			G=sqrt((double)G*0xff);
			B=sqrt((double)B*0xff);
			*p=RGB(R,G,B);
		}
	}
	SetBitmapBits(hBitmap,size,bits);
	free(bits);
	return true;
}
bool RotateImage(string ID,int x,int y,double arc)/**/
{
	map<string,IMAGE>::iterator it;
	if ((it=ImageData.find(ID))==ImageData.end())
		return false;
	HBITMAP hBitmap=it->second.hBitmap;
	int size=it->second.cx*it->second.cy*sizeof(int);
	int *bits=(int*)malloc(size);
	int *temp=(int*)malloc(size);
	GetBitmapBits(hBitmap,size,bits);
	double COS=cos(arc);
	double SIN=sin(arc);
	for (int i=0;i<it->second.cx;i++)
		for (int j=0;j<it->second.cy;j++)
		{
			double rx=i-x;
			double ry=j-y;
			double resrx=rx*COS-ry*SIN;
			double resry=rx*SIN+ry*COS;
			int resx=resrx+x;
			int resy=resry+y;
			if (resx<it->second.cx&&resx>=0&&resy<it->second.cy&&resy>=0)
				*(temp+j*it->second.cx+i)=*(bits+resy*it->second.cx+resx);
			else
				*(temp+j*it->second.cx+i)=0xffffff;
		}
	SetBitmapBits(hBitmap,size,temp);
	free(temp);
	free(bits);
	return true;
}
bool CheckPosition(int x,int y,string imID)/**/
{
	map<string,IMAGE>::iterator it;
	if ((it=ImageData.find(imID))==ImageData.end())
		return false;
	if (!it->second.showed)
		return false;
	IMAGE image=it->second;
	HDC hdc;
	while ((hdc=CreateCompatibleDC(hdcWindow))==NULL)
		Sleep(1);
	DeleteObject(SelectObject(hdc,image.hBitmap));
	int rx=x-image.x;
	int ry=y-image.y;
	if (rx>=image.cx||ry>=image.cy||rx<0||ry<0)
	{
		DeleteDC(hdc);
		return false;
	}
	if (GetPixel(hdc,rx,ry)!=0xffffff)
	{
		DeleteDC(hdc);
		return true;
	}
	else
	{
		DeleteDC(hdc);
		return false;
	}
	
}
bool CopyImage(string sourceID,string destID)/**/
{
	map<string,IMAGE>::iterator source,dest;
	if ((source=ImageData.find(sourceID))==ImageData.end())
		return false;
	if ((dest=ImageData.find(destID))==ImageData.end())
		return false;
	//if (dest->second.showed)
//		return false;
	int size=source->second.cx*source->second.cy*sizeof(int);
	int *bits=(int*)malloc(size);
	GetBitmapBits(source->second.hBitmap,size,bits);
	DeleteObject(dest->second.hBitmap);
	dest->second.hBitmap=CreateBitmap(source->second.cx,source->second.cy,1,32,bits);
	dest->second.cx=source->second.cx;
	dest->second.cy=source->second.cy;
	dest->second.x=-1;
	dest->second.y=-1;
	dest->second.showed=false;
	free(bits);
	return true;	
}
bool CreateImage(string ID,int width,int height)
{
	if (ImageData.find(ID)!=ImageData.end())
		return false;
	IMAGE image;
	image.cx=width;
	image.cy=height;
	image.showed=false;
	image.hBitmap=CreateBitmap(width,height,1,32,NULL);
	ImageData[ID]=image;
	return true;
}
bool LoadAVI(string ID,string FileName)
{
	ifstream fin(FileName.c_str());
	if (!fin)
		return false;
	if (AVIData.find(ID)!=AVIData.end())
		return false;
	int framenum;
	int partnum;
	fin>>framenum>>partnum;
	AVI avi;
	for (int i=1;i<=framenum;i++)
	{
		FRAME frame;
		for (int j=1;j<=partnum;j++)
		{
			PART part;
			string filename;
			fin>>filename>>part.posX>>part.posY>>part.rotateX>>part.rotateY>>part.rotateArc;
			strstream str;
			str<<ID<<"."<<i<<"."<<j;
			str>>part.ImageID;
			frame.parts.insert(frame.parts.end(),part);
			if (!LoadImage(part.ImageID,filename))
				return false;
			if (!RotateImage(part.ImageID,part.rotateX,part.rotateY,part.rotateArc))
				return false;
		}
		avi.frames.insert(avi.frames.end(),frame);
	}
	AVIData[ID]=avi;
	fin.close();
	return true;
}
bool PutAVI(string ID,int frame,int x,int y)
{
	if (AVIData.find(ID)==AVIData.end())
		return false;
	AVI avi=AVIData[ID];
	int framenum=avi.frames.size();
	int partnum=avi.frames[0].parts.size();
	int i=frame-1;
		for (int j=0;j<partnum;j++)
		{
			int rx=avi.frames[i].parts[j].posX+x;
			int ry=avi.frames[i].parts[j].posY+y;
			PutImage(avi.frames[i].parts[j].ImageID,rx,ry);
		}
	return true;
}
int GetAVIFrameCount(string ID)
{
	if (AVIData.find(ID)==AVIData.end())
		return 0;
	return AVIData[ID].frames.size();
}
void ClearScreen()
{
	int *bits=(int *)malloc(MAXSCREENX*MAXSCREENY*sizeof(int));
	memset(bits,0,MAXSCREENX*MAXSCREENY*sizeof(int));
	HBITMAP hbitmap=CreateBitmap(MAXSCREENX,MAXSCREENY,1,32,bits);
	HDC hdc=CreateCompatibleDC(hdcWindow);
	SelectObject(hdc,hbitmap);
	while (Refreshing)
		Sleep(1);
	BitBlt(hdcBuffer,0,0,MAXSCREENX,MAXSCREENY,hdc,0,0,SRCCOPY);
	DeleteDC(hdc);
	DeleteObject(hbitmap);
	free(bits);
}
MOUSESTRUCT GetLastClick()
{
	MOUSESTRUCT res=LastClick;
	LastClick.left=LastClick.right=false;
	return res;
}
extern bool CoreEnded;
void StartGameCore()
{
	SetTimer(hWnd,TIMER_GAMECORE,INTERVAL,NULL);
	CoreEnded=false;
}
void EndGameCore(int delay)
{
	bool temp=ControlRunning;
	ControlRunning=false;
	Sleep(delay);
	while (CoreRunning||CoreWaiting)
		Sleep(1);
	KillTimer(hWnd,TIMER_GAMECORE);
	CoreEnded=true;
	ControlRunning=temp;
}
void DeepenRect(int lx,int ly,int rx,int ry)/**/
{
	int *bits=(int *)malloc(MAXSCREENX*MAXSCREENY*sizeof(int));
	GetBitmapBits(hBitmapBuffer,MAXSCREENX*MAXSCREENY*sizeof(int),bits);
	if (lx<0)
		lx=0;
	if (ly<0)
		ly=0;
	if (rx>=MAXSCREENX)
		rx=MAXSCREENX-1;
	if (ry>=MAXSCREENY)
		ry=MAXSCREENY-1;
	for (int x=lx;x<=rx;x++)
		for (int y=ly;y<=ry;y++)
		{
			int *p=bits+y*MAXSCREENX+x;
			if ((*p)!=0xffffff)
			{
				int B=*p>>16;
				int G=(*p>>8)&0xff;
				int R=*p&0xff;
				R=((double)R/3);
				G=((double)G/3);
				B=((double)B/3);
				*p=RGB(R,G,B);
			}
		}
	while (Refreshing)
		Sleep(1);
	SetBitmapBits(hBitmapBuffer,MAXSCREENX*MAXSCREENY*sizeof(int),bits);
	free(bits);
}
void DrawProcessBar(int x,int y,double rate)
{
	if (rate<0)
		rate=0;
	if (rate>1)
		rate=1;
	static HBITMAP hFull=(HBITMAP)LoadImage(NULL,"data\\processbar2.bmp",IMAGE_BITMAP,157,26,LR_LOADFROMFILE);
	static HBITMAP hBlank=(HBITMAP)LoadImage(NULL,"data\\processbar1.bmp",IMAGE_BITMAP,157,26,LR_LOADFROMFILE);
	int rx=157*rate;
	static HDC hdcFull=CreateCompatibleDC(hdcWindow);
	static HDC hdcBlank=CreateCompatibleDC(hdcWindow);
	DeleteObject(SelectObject(hdcFull,hFull));
	DeleteObject(SelectObject(hdcBlank,hBlank));
	TransparentBlt(hdcBuffer,x,y,rx,26,hdcFull,0,0,rx,26,0xffffff);
	TransparentBlt(hdcBuffer,x+rx,y,157-rx,26,hdcBlank,rx,0,157-rx,26,0xffffff);
}

void Welcome()
{	
	HBITMAP hBitmap =(HBITMAP)LoadImage(NULL,"temp.tmp",IMAGE_BITMAP,800,600,LR_LOADFROMFILE);
	remove("temp.tmp");
	HDC hdc=CreateCompatibleDC(hdcWindow);
	int *bits=(int *)malloc(800*600*sizeof(int));
	GetBitmapBits(hBitmap,800*600*sizeof(int),bits);
	int *temp=(int *)malloc(800*600*sizeof(int));
	memset(temp,0xff,800*600*sizeof(int));
	for (int i=1;i<=60;i++)
	{
		for (int x=0;x<800;x++)
			for (int y=0;y<600;y++)
			{
				int offset=y*MAXSCREENX+x;
				int *p=offset+bits;
				if ((*p)!=0xffffff)
				{
					int B=*p>>16;
					int G=(*p>>8)&0xff;
					int R=*p&0xff;
					R=(255-(255-(double)R)*i/60);
					G=(255-(255-(double)G)*i/60);
					B=(255-(255-(double)B)*i/60);
					*(temp+offset)=RGB(R,G,B);
				}
			}
		SetBitmapBits(hBitmap,800*600*sizeof(int),temp);
		DeleteObject(SelectObject(hdc,hBitmap));
		TransparentBlt(hdcBuffer,0,0,800,600,hdc,0,0,800,600,0xffffff);
	}
	free(bits);
	free(temp);
	DeleteDC(hdc);
	DeleteObject(hBitmap);
	Sleep(2000);
}
void SetWindowTitle(string name)
{
	if (name.length()==0)
		SendMessage(hWnd,WM_SETTEXT,(WPARAM)name.c_str(),(LPARAM)"Plants Vs Zombies");
	else
		SendMessage(hWnd,WM_SETTEXT,(WPARAM)name.c_str(),(LPARAM)("Plants Vs Zombies -"+name).c_str());
}