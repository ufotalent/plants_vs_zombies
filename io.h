#include <windows.h>
#include <process.h>
#include <math.h>
#include <string>
#include <map>
#include <cmath>
#include <fstream>
#include <strstream>
#include "type.h"
using namespace std;
#pragma   comment(lib,"Msimg32.lib")        
#pragma   comment(lib,"Winmm.lib")
void InitGraph();									//initialize graphics
int GetLastKey();									//get the last key pressed and clear buffer
MOUSESTRUCT GetMouse();								//get the mouse status
MOUSESTRUCT GetLastClick();

bool LoadImage(string ID,string FileName);			//load image from file,return true if succeeded
bool PutImage(string ID,int x,int y);				//put image to screen,return true if the image has been loaded
bool DeleteImage(string ID);						//delete images loaded;
bool HighlightImage(string ID);
bool RotateImage(string ID,int x,int y,double arc); //rotate image 
bool CheckPosition(int x,int y,string imID);
bool CopyImage(string sourceID,string destID);		//copy image between two images with same size
bool CreateImage(string ID,int width,int height);
bool LoadAVI(string ID,string FileName);			//
bool PutAVI(string ID,int frame,int x,int y);
void DrawProcessBar(int x,int y,double rate);
int GetAVIFrameCount(string ID);

void ClearScreen();
void SuspendDrawing();								//pause auto refreshing screen to avoid problems
void ResumeDrawing();								//resume refreshing screen
void Refresh();										//refresh the screen
void StartGameCore();
void EndGameCore(int delay);

void DeepenRect(int lx,int ly,int rx,int ry);


void SetWindowTitle(string name);