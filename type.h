#pragma once
#include <string>
#include <vector>
using namespace std;
#define MAXSCREENX 800
#define MAXSCREENY 600
#define MAXSTAGE 5
#define TIMER_REFRESH	0
#define TIMER_GAMECORE  1

#define INTERVAL 10
#define WINDOWNAME "game"
#define DATALIST "data\\datalist.lst"
#define BONUSINTERVAL 100
//modes
#define MD_QUIT 1
#define MD_ADVENTURE 2

#define BACKGROUNDMUSIC "data\\music\\menu.wav"
#define USERLOSTMUSIC "data\\music\\userlost.wav"
#define USERWINMUSIC "data\\music\\userwin.wav"
typedef struct tagMOUSESTRUCT
{
	bool left;
	bool right;
	int x,y;
}MOUSESTRUCT;
typedef struct tagIMAGE
{
	HBITMAP hBitmap;
	int x,y,cx,cy;
	bool showed;
}IMAGE;
typedef struct tagPART
{
	string ImageID;
	int posX,posY,rotateX,rotateY;
	double rotateArc;
}PART;
typedef struct tagFRAME
{
	vector <PART> parts;
}FRAME;
typedef struct tagAVI
{
	vector <FRAME> frames;
}AVI;
typedef struct tagPLANT
{
	int col,row;
	double HP;
	int interval;
	int CoolDown;
	int CurrentCoolDown;
	int Price;
	string TypeID;
	string BulletID;
	string ImageID;
	string SeedID;
	bool SeedHighlighted;
	bool Exploding;
}PLANT;
typedef struct tagZOMBIE
{
	int row;
	double x;
	int HP;
	double speed;
	int attack;
	double frame;
	double attframe;
	bool attacking;
	string TypeID;
	string AVIID;
	string attackAVIID;
	int slow;
}ZOMBIE;
typedef struct tagBULLET
{
	int row;
	double x;
	int attack;
	int speed;
	string TypeID;
	string ImageID;
}BULLET;